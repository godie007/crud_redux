import React from "react";
import { Navbar, Grid, Row, Col } from "react-bootstrap";
import Formulary from "./components/Formulary";
import ListUsers from "./components/ListUsers";
import Loading from "./components/Loading";

import "./App.css";

function App() {
	return (
		<div>
			<Navbar inverse staticTop>
				<Navbar.Header>
					<Navbar.Brand>
						<a href="#">Formulario</a>
					</Navbar.Brand>
				</Navbar.Header>
			</Navbar>
			<Grid>
				<Row>
					<Col xs={6} md={4} >
						<Formulary />
						<Loading />
					</Col>
					<Col xs={6} md={8} >
						<ListUsers />
					</Col>
				</Row>
			</Grid>
		</div>
	);
}


export default App;
