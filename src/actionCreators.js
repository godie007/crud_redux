import axios from "axios";
import {
	ADD_USER_SUCCESS,
	EDIT_USER_SUCCESS,
	DELETE_USER_SUCCESS,
	CLEAR_FORM,
	CHANGE_VALUE,
	SHOW_USER,
	LIST_USERS,
	ADD_USER_REQUEST,
	EDIT_USER_REQUEST,
	DELETE_USER_REQUEST,
	ADD_USER_FAILURE,
	EDIT_USER_FAILURE,
	DELETE_USER_FAILURE,
} from "./constants/index";

export function loadUsers() {
	return dispatch => {
		return axios.get(getURL("users/listar/"))
			.then(users => {
				dispatch({ type: LIST_USERS, users});
			});
	};
};

export function valueChange(name, value) {
	return { type: CHANGE_VALUE, field: name, value };
}

export function addUser(user) {
	return dispatch => {
		request(dispatch, ADD_USER_REQUEST);
		return axios.post(getURL("users"),{
			user
		})
			.then(res => {console.log(res.data);requestSuccess(ADD_USER_SUCCESS, dispatch, res.data);})
			.catch(error => requestFailure (ADD_USER_FAILURE,dispatch, error));
	};
}

export function editUser(user) {
	return dispatch => {
		request(dispatch, EDIT_USER_REQUEST);
		return axios.put(getURL("users"),{
			user
		})
			.then(u => requestSuccess(EDIT_USER_SUCCESS, dispatch,user))
			.catch(error => requestFailure (EDIT_USER_FAILURE,dispatch, error));
	};
}
export function deleteUser(user) {
	return dispatch => {
		request(dispatch, DELETE_USER_REQUEST);
		return axios.delete(getURL("users"),{
			data: { _id: user._id }
		})
			.then(u => requestSuccess(DELETE_USER_SUCCESS, dispatch,user))
			.catch(error => requestFailure (DELETE_USER_FAILURE,dispatch, error));
	};
}

export function clearForm(dispatch) {
	return dispatch({
		type: CLEAR_FORM
	});
}

export function showUser(user) {
	return { type: SHOW_USER, user };
}

function requestSuccess (constant,dispatch, user) {
	dispatch({ type: constant, user });
	clearForm(dispatch);
}

function requestFailure (constant,dispatch, error) {
	dispatch({ type: constant, });
	clearForm(dispatch);
}

function request(dispatch,constant){
	dispatch({ type: constant});
}

function getURL(endPoint){
	return `http://192.168.88.48:3001/${endPoint}`;
}
