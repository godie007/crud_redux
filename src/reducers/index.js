import { combineReducers } from "redux";
import form from "./form";
import users from "./user";
import events from "./events";

export default combineReducers({
	form,
	users,
	events,
});
