
import {
	ADD_USER_FAILURE,
	EDIT_USER_FAILURE,
	DELETE_USER_FAILURE,
	ADD_USER_REQUEST,
	DELETE_USER_REQUEST,
	EDIT_USER_REQUEST,
	ADD_USER_SUCCESS,
	EDIT_USER_SUCCESS,
	DELETE_USER_SUCCESS,
} from "../constants";

const initialState = {
	error:false, isFetching:false, message: "",isSuccess:false };

export default function form(state = initialState, action) {
	switch (action.type) {
	case ADD_USER_FAILURE:
		return {...state,
			error:true,
			message:action.payload.message};
	case EDIT_USER_FAILURE:
		return {...state,
			error:true,
			message:action.payload.message};
	case DELETE_USER_FAILURE:
		return {...state,
			error:true,
			message:action.payload.message};
	case ADD_USER_REQUEST:
		return {...state,
			isFetching:true,
			message:"Loading..."
		};
	case EDIT_USER_REQUEST:
		return {...state,
			isFetching:true,
			message:"Loading..."
		};
	case DELETE_USER_REQUEST:
		return {...state,
			isFetching:true,
			message:"Loading..."
		};
	case ADD_USER_SUCCESS:
		return {...state,
			isFetching:false,
			isSuccess:true,
			message:"Added successfully"};
	case EDIT_USER_SUCCESS:
		return {...state,
			isFetching:false,
			isSuccess:true,
			message:"Edit successfully"};
	case DELETE_USER_SUCCESS:
		return {...state,
			isFetching:false,
			isSuccess:true,
			message:"Delete successfully"};
	default:
		return state;
	}
}
