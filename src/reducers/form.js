
import {
	CLEAR_FORM,
	CHANGE_VALUE,
	SHOW_USER,
} from "../constants";

const initialState = { id:"", name: "", email: "", description: "" };

export default function form(state = initialState, action) {
	switch (action.type) {
	case CHANGE_VALUE:
		return { ...state, [action.field]: action.value };
	case CLEAR_FORM:
		return { ...initialState };
	case SHOW_USER:
		return { ...action.user };
	default:
		return state;
	}
}
