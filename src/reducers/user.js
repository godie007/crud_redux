
import {
	ADD_USER_SUCCESS,
	EDIT_USER_SUCCESS,
	DELETE_USER_SUCCESS,
	LIST_USERS,
} from "../constants";

const initialState = [];

export default function user(state = initialState, action) {
	switch (action.type) {
	case ADD_USER_SUCCESS:
		return state.concat(action.user);
	case DELETE_USER_SUCCESS:
		return state.filter(u => u !== action.user);
	case EDIT_USER_SUCCESS:
		return state.map(u => u._id === action.user._id ? action.user : u);
	case LIST_USERS:
		return action.users.data;
	default:
		return state;
	}
}
