import React,{PropTypes} from "react";
import { connect } from "react-redux";
import { Alert } from "react-bootstrap";

const Loading = ({error,message,isFetching,isSuccess}) => (
	<div>
		{error && <Alert bsStyle="warning">
			<strong>Error!</strong> {message}
		</Alert>
		}
		{isFetching && <Alert bsStyle="warning">
			<div>{message}</div>
		</Alert>
		}
		{isSuccess && <Alert bsStyle="success">
			<div>{message}</div>
		</Alert>
		}
	</div>

);

Loading.propTypes = {
	message: PropTypes.string,
	isFetching: PropTypes.bool,
	isSuccess: PropTypes.bool,
	error: PropTypes.bool,
};

const mapStateToProps = state => ({
	message: state.events.message,
	isFetching: state.events.isFetching,
	isSuccess: state.events.isSuccess,
});

export default connect(mapStateToProps, null)(Loading);
