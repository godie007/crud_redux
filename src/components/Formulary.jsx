import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { valueChange, addUser, loadUsers, editUser } from "../actionCreators";
import { Panel, Button } from "react-bootstrap";

class Formulaty extends React.Component {
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.handrlerSave = this.handrlerSave.bind(this);
		this.handrlerUpdate = this.handrlerUpdate.bind(this);
	}

	componentDidMount(){
		const { actions } = this.props;
		actions.loadUsers();
	}

	handleChange(e) {
		const { actions } = this.props;
		const name = e.target.name;
		const value = e.target.value;
		actions.valueChange(name, value);
	}

	handrlerSave() {
		const { actions, name, description, email } = this.props;
		const user = {
			name, description, email,
		};
		actions.addUser(user);
	}
	handrlerUpdate() {
		const { actions, _id , name, description, email } = this.props;
		const user = {
			_id, name, description, email,
		};
		actions.editUser(user);
	}

	render() {
		const { _id, name, email, description } = this.props;
		return (
			<Panel header={"Formulario"}>
				<form onSubmit={e => e.preventDefault()} >
					<div className="control-group">
						<label>
              Nombre
						</label>
						<div className="controls">
							<input type="text" name="name" value={name} onChange={(e)=>this.handleChange(e)} />
						</div>
					</div>
					<div className="control-group">
						<label>
              Correo
						</label>
						<div className="controls">
							<input type="text" name="email" value={email} onChange={(e)=>this.handleChange(e)} />
						</div>
					</div>
					<div className="control-group">
						<label>
              Descripción
						</label>
						<div className="controls">
							<textarea type="text" name="description" value={description} onChange={(e)=>this.handleChange(e)} />
						</div>
						{_id === undefined ?
							<Button className="btn btn-primary" onClick={this.handrlerSave}>Guardar</Button>
							:
							<Button className="btn btn-success" onClick={this.handrlerUpdate}>Actualizar</Button>
						}

					</div>
				</form>
			</Panel>
		);
	}
}

Formulaty.propTypes = {
	actions: PropTypes.object,
	name: PropTypes.string,
	email: PropTypes.string,
	description: PropTypes.string,
};

const mapStateToProps = state => ({
	_id: state.form._id,
	name: state.form.name,
	email: state.form.email,
	description: state.form.description,
});

const mapDispatchToProps = dispatch => ({
	actions: {
		valueChange: (name, value) => dispatch(valueChange(name, value)),
		addUser: user => dispatch(addUser(user)),
		loadUsers: () => dispatch(loadUsers()),
		editUser: user => dispatch(editUser(user)),
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(Formulaty);


