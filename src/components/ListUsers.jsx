import React from "react";
import { connect } from "react-redux";
import { deleteUser, showUser } from "../actionCreators";
import { Panel, Table, Button, Glyphicon } from "react-bootstrap";

const ListUsers = ({ user, actions }) => (
	<Panel header="Lista de Usuarios">
		<Table fill>
			<thead>
				<tr>
					<th>N°</th>
					<th>Nombre</th>
					<th>Correo</th>
					<th>Descripción</th>
					<th>Acción</th>
				</tr>
			</thead>
			<tbody>
				{user.map((us, index) =>
					(<tr key={index}>
						<td>{index}</td>
						<td>{us.name}</td>
						<td>{us.email}</td>
						<td>{us.description}</td>
						<td>
							<Button bsSize="xsmall" bsStyle="success" onClick={() => actions.handlerShow(us)} >
								<Glyphicon glyph="edit" />
							</Button>
						</td>
						<td>
							<Button bsSize="xsmall" bsStyle="default" onClick={() => actions.handlerDelete(us)} >
								<Glyphicon glyph="trash" />
							</Button>
						</td>
					</tr>)
				)}
			</tbody>
		</Table>
	</Panel>
);


const mapStateToProps = state => ({
	user: state.users,
});
const mapDispatchToProps = dispatch => ({
	actions: {
		handlerDelete: user => dispatch(deleteUser(user)),
		handlerShow: user => dispatch(showUser(user)),
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(ListUsers);
